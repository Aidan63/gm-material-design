///ui_list_create(x, y, width, height);

var x1 = argument[0];
var y1 = argument[1];
var w  = argument[2];
var h  = argument[3];

var obj = instance_create(x1, y1, obj_ui_list);

obj.ui_x      = x1;
obj.ui_y      = y1;
obj.ui_width  = w;
obj.ui_height = h;

return obj;
