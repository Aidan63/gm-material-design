///ui_set_style(element id, background colour, line 1 text colour, line 2 text colour, line 1 font, line 2 font, flat, ink colour, ink alpha, ink radius, ink extra expand);

var obj      = argument[0];
var bkgCol   = argument[1];
var l1txtCol = argument[2];
var l2txtCol = argument[3];
var l1txtFnt = argument[4];
var l2txtFnt = argument[5];
var flat     = argument[6];
var inkCol   = argument[7];
var inkAlpha = argument[8];
var inkRad   = argument[9];
var extraExp = argument[10];

obj.ui_bkgColour       = bkgCol;
obj.ui_textLine1Colour = l1txtCol;
obj.ui_textLine2Colour = l2txtCol;
obj.ui_textLine1Font   = l1txtFnt;
obj.ui_textLine2Font   = l2txtFnt;
obj.ui_flat            = flat;
obj.ui_inkColour       = inkCol;
obj.ui_inkAlpha        = inkAlpha;
obj.ui_inkRadius       = inkRad;
obj.ui_inkExtraExpand  = extraExp;
