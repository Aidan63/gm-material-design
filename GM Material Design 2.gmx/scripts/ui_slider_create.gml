///ui_slider_create(x, y, width, start x, colour);

var x1  = argument[0];
var y1  = argument[1];
var w   = argument[2];
var sx  = argument[3];
var col = argument[4];

var obj = instance_create(x1, y1, obj_ui_slider);
obj.ui_x = x1;
obj.ui_y = y1;
obj.ui_width        = w;
obj.ui_sliderStartx = sx;
obj.ui_sliderColour = col;
