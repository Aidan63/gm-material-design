///ui_toggle_create(x, y, width);

var x1 = argument[0];
var y1 = argument[1];
var w  = argument[2];

var obj = instance_create(x1, y1, obj_ui_toggle);

obj.ui_x     = x1;
obj.ui_y     = y1;
obj.ui_width = w;

return obj;

