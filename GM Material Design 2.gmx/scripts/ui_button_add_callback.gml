///ui_button_add_callback(ui element, action, script, arg0, arg1);

var element = argument[0];
var action  = argument[1];
var script  = argument[2];
var _index  = 0;

if (action = 0)
{
    element.ui_scriptPressed = asset_get_index(script);
    
    repeat (argument_count - 3)
    {
        element.ui_scriptPressedArg[_index] = argument[_index + 3];
        _index ++;
    }
}
else
{
    element.ui_scriptReleased = asset_get_index(script);
    
    repeat (argument_count - 3)
    {
        element.ui_scriptReleasedArg[_index] = argument[_index + 3];
        _index ++;
    }
}

