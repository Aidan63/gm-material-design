///ui_list_add_callback(element, list item, action, script, arguments);

var element = argument[0];
var item    = argument[1];
var action  = argument[2];
var script  = argument[3];
var _index  = 0;

if (action == 0)
{
    element.ui_scriptPressed[item] = asset_get_index(script);
    
    repeat (argument_count - 4)
    {
        element.ui_scriptPressedArg[item, _index] = argument[_index + 4];
        _index ++;
    }
}
else
{
    element.ui_scriptReleased[item] = asset_get_index(script);
    
    repeat (argument_count - 4)
    {
        element.ui_scriptReleasedArg[item, _index] = argument[_index + 4];
        _index ++;
    }
}

