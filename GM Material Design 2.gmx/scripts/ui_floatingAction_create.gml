///ui_floatingAction_create(x, y, radius, colour, sprite, sub image);

var x1  = argument[0];
var y1  = argument[1];
var rad = argument[2];
var col = argument[3];
var img = argument[4];
var sub = argument[5];

var obj = instance_create(x1, y1, obj_ui_floatingAction);

obj.ui_x          = x1;
obj.ui_y          = y1;
obj.ui_radius     = rad;
obj.ui_bkgColour  = col;
obj.ui_imageID    = img;
obj.ui_imageIndex = sub;

