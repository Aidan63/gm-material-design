///ui_set_content(element id, image, text line 1, text line 2);

var obj = argument[0];
var img = argument[1];
var txtl1 = argument[2];
var txtl2 = argument[3];

obj.ui_imageID = img;
obj.ui_textLine1 = txtl1;
obj.ui_textLine2 = txtl2;