///ui_list_add_item(element ID, image ID, sub image, text);

obj = argument[0];
img = argument[1];
sub = argument[2];
txt = argument[3];

var element = obj;

with (element)
{    
    ui_contentMap[ui_contentSize, 0] = other.img;
    ui_contentMap[ui_contentSize, 1] = other.sub;
    ui_contentMap[ui_contentSize, 2] = other.txt;
    
    ui_contentSize ++;
}