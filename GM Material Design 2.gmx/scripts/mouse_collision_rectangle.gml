///mouse_collision_rectangle(x, y, w, h);

var x1 = argument[0];
var y1 = argument[1];
var w  = argument[2];
var h  = argument[3];
var x2 = x1 + w;
var y2 = y1 + h;

if (mouse_x > x1 && mouse_y > y1 && mouse_x < x2 && mouse_y < y2)
{
    return true;
}
else
{
    return false;
}