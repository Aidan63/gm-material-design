/// InitBC()
global.window_has_border = true;

global._RemoveBorder = external_define("RemoveBorder.dll", "RemoveBorder", dll_cdecl, ty_real, 0);
global._RestoreBorder = external_define("RemoveBorder.dll", "RestoreBorder", dll_cdecl, ty_real, 0);

var _InitBorder = external_define("RemoveBorder.dll", "InitBorderControl", dll_cdecl, ty_real, 1, ty_real);

return external_call(_InitBorder, window_handle());