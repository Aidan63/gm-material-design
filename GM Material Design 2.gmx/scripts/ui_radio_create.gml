///ui_radio_create(x, y);

var x1 = argument[0];
var y1 = argument[1];

var obj = instance_create(x1, y1, obj_ui_radio);

obj.ui_x = x1;
obj.ui_y = y1;

return obj;

