///ui_set_align(element id, text align, space from paper edge, text image space);

var obj    = argument[0];
var txtAli = argument[1];
var pEdge  = argument[2];
var imgSpc = argument[3];

obj.ui_textAlign      = txtAli;
obj.ui_edgeSpace      = pEdge;
obj.ui_textImageSpace = imgSpc;
